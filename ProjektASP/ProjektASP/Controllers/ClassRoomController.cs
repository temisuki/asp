﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ProjektASP.Models;

namespace ProjektASP.Controllers
{
    public class ClassRoomController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/ClassRoom
        public IQueryable<ClassRoom> GetClassRooms()
        {
            return db.ClassRooms;
        }

        // GET: api/ClassRoom/5
        [ResponseType(typeof(ClassRoom))]
        public IHttpActionResult GetClassRoom(int id)
        {
            ClassRoom classRoom = db.ClassRooms.Find(id);
            if (classRoom == null)
            {
                return NotFound();
            }

            return Ok(classRoom);
        }

        // PUT: api/ClassRoom/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutClassRoom(int id, ClassRoom classRoom)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != classRoom.ClassRoomID)
            {
                return BadRequest();
            }

            db.Entry(classRoom).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClassRoomExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ClassRoom
        [ResponseType(typeof(ClassRoom))]
        public IHttpActionResult PostClassRoom(ClassRoom classRoom)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ClassRooms.Add(classRoom);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = classRoom.ClassRoomID }, classRoom);
        }

        // DELETE: api/ClassRoom/5
        [ResponseType(typeof(ClassRoom))]
        public IHttpActionResult DeleteClassRoom(int id)
        {
            ClassRoom classRoom = db.ClassRooms.Find(id);
            if (classRoom == null)
            {
                return NotFound();
            }

            db.ClassRooms.Remove(classRoom);
            db.SaveChanges();

            return Ok(classRoom);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClassRoomExists(int id)
        {
            return db.ClassRooms.Count(e => e.ClassRoomID == id) > 0;
        }
    }
}