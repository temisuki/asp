﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjektASP.Models;

namespace ProjektASP.Controllers
{
    public class MarksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Marks
        public ActionResult Index()
        {
            var marks = db.Marks.Include(m => m.StudentModel).Include(m => m.SubjectModel);
            return View(marks.ToList());
        }

        // GET: Marks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mark mark = db.Marks.Find(id);
            if (mark == null)
            {
                return HttpNotFound();
            }
            return View(mark);
        }

        // GET: Marks/Create
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Create()
        {
            ViewBag.StudentID = new SelectList(db.StudentModels, "StudentID", "Name");
            ViewBag.SubjectID = new SelectList(db.SubjectModels, "SubjectID", "Name");
            return View();
        }

        // POST: Marks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Create([Bind(Include = "MarkID,MarkNum,StudentID,SubjectID")] Mark mark)
        {
            if (ModelState.IsValid)
            {
                db.Marks.Add(mark);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.StudentID = new SelectList(db.StudentModels, "StudentID", "Name", mark.StudentID);
            ViewBag.SubjectID = new SelectList(db.SubjectModels, "SubjectID", "Name", mark.SubjectID);
            return View(mark);
        }

        // GET: Marks/Edit/5
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mark mark = db.Marks.Find(id);
            if (mark == null)
            {
                return HttpNotFound();
            }
            ViewBag.StudentID = new SelectList(db.StudentModels, "StudentID", "Name", mark.StudentID);
            ViewBag.SubjectID = new SelectList(db.SubjectModels, "SubjectID", "Name", mark.SubjectID);
            return View(mark);
        }

        // POST: Marks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Edit([Bind(Include = "MarkID,MarkNum,StudentID,SubjectID")] Mark mark)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mark).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.StudentID = new SelectList(db.StudentModels, "StudentID", "Name", mark.StudentID);
            ViewBag.SubjectID = new SelectList(db.SubjectModels, "SubjectID", "Name", mark.SubjectID);
            return View(mark);
        }

        // GET: Marks/Delete/5
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mark mark = db.Marks.Find(id);
            if (mark == null)
            {
                return HttpNotFound();
            }
            return View(mark);
        }

        // POST: Marks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult DeleteConfirmed(int id)
        {
            Mark mark = db.Marks.Find(id);
            db.Marks.Remove(mark);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
