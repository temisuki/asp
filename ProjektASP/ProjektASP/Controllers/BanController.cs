﻿using ProjektASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProjektASP.Controllers
{
    public class BanController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Ban
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            var usersList = db.Users.OrderBy(r => r.UserName).ToList().Select(rr => new SelectListItem { Value = rr.UserName.ToString(), Text = rr.UserName }).ToList();

            ViewBag.Users = usersList;
            return View();
        }
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Ban(string UserName)
        {
            ApplicationUser user = db.Users.Where(u => u.UserName.Equals(UserName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            user.LockoutEnabled = true;
            user.LockoutEndDateUtc = DateTime.UtcNow.AddMinutes(60);
            ViewBag.Message = "Użytkownik został zbanowany";
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }
    }
}