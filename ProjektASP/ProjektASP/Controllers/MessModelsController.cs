﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjektASP.Models;

namespace ProjektASP.Controllers
{
    public class MessModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: MessModels
        public ActionResult Index()
        {
            return View(db.MessModels.ToList());
        }

        // GET: MessModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MessModel messModel = db.MessModels.Find(id);
            if (messModel == null)
            {
                return HttpNotFound();
            }
            return View(messModel);
        }

        // GET: MessModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MessModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MessID,Topic,Mess")] MessModel messModel)
        {
            if (ModelState.IsValid)
            {
                db.MessModels.Add(messModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(messModel);
        }

        // GET: MessModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MessModel messModel = db.MessModels.Find(id);
            if (messModel == null)
            {
                return HttpNotFound();
            }
            return View(messModel);
        }

        // POST: MessModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MessID,Topic,Mess")] MessModel messModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(messModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(messModel);
        }

        // GET: MessModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MessModel messModel = db.MessModels.Find(id);
            if (messModel == null)
            {
                return HttpNotFound();
            }
            return View(messModel);
        }

        // POST: MessModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MessModel messModel = db.MessModels.Find(id);
            db.MessModels.Remove(messModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
