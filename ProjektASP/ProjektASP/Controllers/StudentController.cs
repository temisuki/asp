﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ProjektASP.Models;
using System.Threading.Tasks;

namespace ProjektASP.Controllers
{
    public class StudentController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET api/student
        public IQueryable<StudentDTO> GetStudents()
        {
            var students = from s in db.StudentModels
                        select new StudentDTO()
                        {
                            StudentID = s.StudentID,
                            Name = s.Name,
                            Surname = s.Surname,
                            ClassRoomName = s.ClassRoom.Name
                        };
            return students;
        }

        // GET: api/Student/5
        [ResponseType(typeof(StudentDetailDTO))]
        public async Task<IHttpActionResult> GetStudent(int id)
        {
            var student = await db.StudentModels.Include(s => s.ClassRoom).Select(s =>
                new StudentDetailDTO()
                {
  
                    StudentID = s.StudentID,
                    Name = s.Name,
                    Surname = s.Surname,
                    Adress = s.Adress,
                    PhoneNumber = s.PhoneNumber,
                    Age = s.Age,
                    Gender = s.Gender,
                    ClassRoomName = s.ClassRoom.Name
                }).SingleOrDefaultAsync(s => s.StudentID == id);
            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }

        // PUT: api/Student/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutStudentModel(int id, StudentModel studentModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != studentModel.StudentID)
            {
                return BadRequest();
            }

            db.Entry(studentModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(StudentModel))]
        public async Task<IHttpActionResult> PostBook(StudentModel studentModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.StudentModels.Add(studentModel);
            await db.SaveChangesAsync();

            // Load author name
            db.Entry(studentModel).Reference(x => x.ClassRoom).Load();

            var dto = new StudentDTO()
            {
                StudentID = studentModel.StudentID,
                Name = studentModel.Name,
                Surname = studentModel.Surname,
                ClassRoomName = studentModel.ClassRoom.Name
            };

            return CreatedAtRoute("DefaultApi", new { id = studentModel.StudentID }, dto);
        }

        // DELETE: api/Student/5
        [ResponseType(typeof(StudentModel))]
        public IHttpActionResult DeleteStudentModel(int id)
        {
            StudentModel studentModel = db.StudentModels.Find(id);
            if (studentModel == null)
            {
                return NotFound();
            }

            db.StudentModels.Remove(studentModel);
            db.SaveChanges();

            return Ok(studentModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StudentModelExists(int id)
        {
            return db.StudentModels.Count(e => e.StudentID == id) > 0;
        }
    }
}