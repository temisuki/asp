﻿using ProjektASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace ProjektASP.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index(int? page)
        {
            var Mess = from s in db.MessModels
                           select s;
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            Mess = Mess.OrderBy(s => s.Topic);
            return View(Mess.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Api()
        {
            return View();
        }
    }
}