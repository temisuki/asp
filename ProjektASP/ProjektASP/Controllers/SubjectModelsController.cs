﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjektASP.Models;

namespace ProjektASP.Controllers
{
    public class SubjectModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SubjectModels
        public ActionResult Index()
        {
            return View(db.SubjectModels.ToList());
        }

        // GET: SubjectModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubjectModel subjectModel = db.SubjectModels.Find(id);
            if (subjectModel == null)
            {
                return HttpNotFound();
            }
            return View(subjectModel);
        }

        // GET: SubjectModels/Create
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: SubjectModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Create([Bind(Include = "SubjectID,Name")] SubjectModel subjectModel)
        {
            if (ModelState.IsValid)
            {
                db.SubjectModels.Add(subjectModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(subjectModel);
        }

        // GET: SubjectModels/Edit/5
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubjectModel subjectModel = db.SubjectModels.Find(id);
            if (subjectModel == null)
            {
                return HttpNotFound();
            }
            return View(subjectModel);
        }

        // POST: SubjectModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Edit([Bind(Include = "SubjectID,Name")] SubjectModel subjectModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subjectModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(subjectModel);
        }

        // GET: SubjectModels/Delete/5
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubjectModel subjectModel = db.SubjectModels.Find(id);
            if (subjectModel == null)
            {
                return HttpNotFound();
            }
            return View(subjectModel);
        }

        // POST: SubjectModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult DeleteConfirmed(int id)
        {
            SubjectModel subjectModel = db.SubjectModels.Find(id);
            db.SubjectModels.Remove(subjectModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
