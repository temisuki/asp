﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjektASP.Models;

namespace ProjektASP.Controllers
{
    public class StudentModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: StudentModels
        public ActionResult Index()
        {
            var studentModels = db.StudentModels.Include(s => s.ClassRoom);
            return View(studentModels.ToList());
        }

        // GET: StudentModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentModel studentModel = db.StudentModels.Find(id);
            if (studentModel == null)
            {
                return HttpNotFound();
            }
            return View(studentModel);
        }

        // GET: StudentModels/Create
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Create()
        {
            ViewBag.ClassRoomID = new SelectList(db.ClassRooms, "ClassRoomID", "Name");
            return View();
        }

        // POST: StudentModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Create([Bind(Include = "StudentID,Name,Surname,Adress,PhoneNumber,Age,Gender,ClassRoomID")] StudentModel studentModel)
        {
            if (ModelState.IsValid)
            {
                db.StudentModels.Add(studentModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClassRoomID = new SelectList(db.ClassRooms, "ClassRoomID", "Name", studentModel.ClassRoomID);
            return View(studentModel);
        }

        // GET: StudentModels/Edit/5
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentModel studentModel = db.StudentModels.Find(id);
            if (studentModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassRoomID = new SelectList(db.ClassRooms, "ClassRoomID", "Name", studentModel.ClassRoomID);
            return View(studentModel);
        }

        // POST: StudentModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Edit([Bind(Include = "StudentID,Name,Surname,Adress,PhoneNumber,Age,Gender,ClassRoomID")] StudentModel studentModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(studentModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClassRoomID = new SelectList(db.ClassRooms, "ClassRoomID", "Name", studentModel.ClassRoomID);
            return View(studentModel);
        }

        // GET: StudentModels/Delete/5
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentModel studentModel = db.StudentModels.Find(id);
            if (studentModel == null)
            {
                return HttpNotFound();
            }
            return View(studentModel);
        }

        // POST: StudentModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Nauczyciel")]
        public ActionResult DeleteConfirmed(int id)
        {
            StudentModel studentModel = db.StudentModels.Find(id);
            db.StudentModels.Remove(studentModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
