﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace ProjektASP
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Application["Visited"] = 0;
        }
        protected void Session_Start()
        {
            Application.Lock();
            Application["Visited"] = (int)Application["Visited"] + 1;
            Application.UnLock();
        }

        protected void Application_OnAuthenticateReuqest(object sender, EventArgs e)
        {
            if (!UserHasAccess())
            {
                FormsAuthentication.SignOut();
            }
        }

        private bool UserHasAccess()
        {
            var user = Membership.GetUser(Context.User.Identity);

            return user.IsApproved;
        }
    }
}
