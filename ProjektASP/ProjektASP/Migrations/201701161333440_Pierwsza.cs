namespace ProjektASP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Pierwsza : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SchoolModels",
                c => new
                    {
                        SchoolID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Adress = c.String(nullable: false, maxLength: 100),
                        Chef = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.SchoolID);
            
            AddColumn("dbo.ClassRooms", "SchoolID", c => c.Int(nullable: false));
            CreateIndex("dbo.ClassRooms", "SchoolID");
            AddForeignKey("dbo.ClassRooms", "SchoolID", "dbo.SchoolModels", "SchoolID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClassRooms", "SchoolID", "dbo.SchoolModels");
            DropIndex("dbo.ClassRooms", new[] { "SchoolID" });
            DropColumn("dbo.ClassRooms", "SchoolID");
            DropTable("dbo.SchoolModels");
        }
    }
}
