namespace ProjektASP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Trzecia : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StudentModels", "ClassRoom_ClassRoomID", "dbo.ClassRooms");
            DropIndex("dbo.StudentModels", new[] { "ClassRoom_ClassRoomID" });
            RenameColumn(table: "dbo.StudentModels", name: "ClassRoom_ClassRoomID", newName: "ClassRoomID");
            AlterColumn("dbo.StudentModels", "ClassRoomID", c => c.Int(nullable: false));
            CreateIndex("dbo.StudentModels", "ClassRoomID");
            AddForeignKey("dbo.StudentModels", "ClassRoomID", "dbo.ClassRooms", "ClassRoomID", cascadeDelete: true);
            DropColumn("dbo.StudentModels", "ClassID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.StudentModels", "ClassID", c => c.Int(nullable: false));
            DropForeignKey("dbo.StudentModels", "ClassRoomID", "dbo.ClassRooms");
            DropIndex("dbo.StudentModels", new[] { "ClassRoomID" });
            AlterColumn("dbo.StudentModels", "ClassRoomID", c => c.Int());
            RenameColumn(table: "dbo.StudentModels", name: "ClassRoomID", newName: "ClassRoom_ClassRoomID");
            CreateIndex("dbo.StudentModels", "ClassRoom_ClassRoomID");
            AddForeignKey("dbo.StudentModels", "ClassRoom_ClassRoomID", "dbo.ClassRooms", "ClassRoomID");
        }
    }
}
