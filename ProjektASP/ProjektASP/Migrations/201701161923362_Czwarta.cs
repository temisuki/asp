namespace ProjektASP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Czwarta : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MessModels",
                c => new
                    {
                        MessID = c.Int(nullable: false, identity: true),
                        Topic = c.String(nullable: false, maxLength: 15),
                        Mess = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.MessID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MessModels");
        }
    }
}
