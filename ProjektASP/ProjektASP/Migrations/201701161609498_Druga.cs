namespace ProjektASP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Druga : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Marks",
                c => new
                    {
                        MarkID = c.Int(nullable: false, identity: true),
                        MarkNum = c.Int(nullable: false),
                        StudentID = c.Int(nullable: false),
                        SubjectID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MarkID)
                .ForeignKey("dbo.StudentModels", t => t.StudentID, cascadeDelete: true)
                .ForeignKey("dbo.SubjectModels", t => t.SubjectID, cascadeDelete: true)
                .Index(t => t.StudentID)
                .Index(t => t.SubjectID);
            
            CreateTable(
                "dbo.SubjectModels",
                c => new
                    {
                        SubjectID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.SubjectID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Marks", "SubjectID", "dbo.SubjectModels");
            DropForeignKey("dbo.Marks", "StudentID", "dbo.StudentModels");
            DropIndex("dbo.Marks", new[] { "SubjectID" });
            DropIndex("dbo.Marks", new[] { "StudentID" });
            DropTable("dbo.SubjectModels");
            DropTable("dbo.Marks");
        }
    }
}
