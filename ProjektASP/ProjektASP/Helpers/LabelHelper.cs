﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjektASP.Helpers
{
    public static class LabelHelper
    {
        public static IHtmlString CustomLabel(MvcHtmlString Content)
        {
            string LableStr = $"<center><label style=\"background-color:gray;color:yellow;font-size:24px\">{Content}</label></center>";
            return new HtmlString(LableStr);
        }
    }
}