﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektASP.Models
{
    public class SubjectModel
    {
        [Key]
        public int SubjectID { get; set; }

        [Required]
        [Display(Name = "Nazwa przedmiotu")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} liter.", MinimumLength = 6)]
        public string Name { get; set; }
    }
}