﻿using ProjektASP.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektASP.Models
{
    public class StudentModel
    {
        [Key]
        public int StudentID { get; set; }

        [Required]
        [Display(Name = "Imię")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} znaki.", MinimumLength = 5)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Nazwisko")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} znaki.", MinimumLength = 5)]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "Adres")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} znaki.", MinimumLength = 5)]
        public string Adress { get; set; }

        [Required]
        [Display(Name = "Numer kontaktowy (rodzic lub opiekun prawny)")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} znaki.", MinimumLength = 5)]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Wiek")]
        [AGEV]
        public int Age { get; set; }

        [Required]
        [Display(Name = "Gender")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} litery.", MinimumLength = 3)]
        public string Gender { get; set; }

        public int ClassRoomID { get; set; }
        public ClassRoom ClassRoom { get; set; }
    }
}