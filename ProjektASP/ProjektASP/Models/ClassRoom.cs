﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektASP.Models
{
    public class ClassRoom
    {
        [Key]
        public int ClassRoomID { get; set; }

        [Required]
        [Display(Name = "Klasa")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} liter.", MinimumLength = 6)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Rocznik")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} liter.", MinimumLength = 6)]
        public string Years { get; set; }

        public int SchoolID { get; set; }
        public SchoolModel SchoolModel { get; set; }
    }
}