﻿using ProjektASP.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektASP.Models
{
    public class Mark
    {
        [Key] 
        public int MarkID { get; set; }

        [Required]
        [Display(Name = "Ocena")]
        [MARKV]
        public int MarkNum { get; set; }

        public int StudentID { get; set; }
        public StudentModel StudentModel { get; set; }

        public int SubjectID { get; set; }
        public SubjectModel SubjectModel { get; set; }
    }
}