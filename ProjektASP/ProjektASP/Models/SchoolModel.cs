﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektASP.Models
{
    public class SchoolModel
    {
        [Key]
        public int SchoolID { get; set; }

        [Required]
        [Display(Name = "Nazwa Szkoły")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} liter.", MinimumLength = 6)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Adres")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} liter.", MinimumLength = 6)]
        public string Adress { get; set; }

        [Required]
        [Display(Name = "Dyrektor")]
        [StringLength(100, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} liter.", MinimumLength = 6)]
        public string Chef { get; set; }
    }
}