﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektASP.Models
{
    public class MessModel
    {
        [Key]
        public int MessID { get; set; }

        [Required]
        [Display(Name = "Temat")]
        [StringLength(15, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} liter.", MinimumLength = 3)]
        public string Topic { get; set; }

        [Required]
        [Display(Name = "Wiadomość")]
        [StringLength(150, ErrorMessage = "Pole {0} musi zawierać przynajmniej {2} liter.", MinimumLength = 6)]
        public string Mess { get; set; }
    }
}