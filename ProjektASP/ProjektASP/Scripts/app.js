﻿var ViewModel = function () {
    var self = this;
    self.students = ko.observableArray();
    self.error = ko.observable();
    self.detail = ko.observable();
    self.classRooms = ko.observableArray();
    self.newStudent = {
        ClassRoom: ko.observable(),
        Name: ko.observable(),
        Surname: ko.observable(),
        Adress: ko.observable(),
        PhoneNumber: ko.observable(),
        Age: ko.observable(),
        Gender: ko.observable(),

    }

    var studentsUri = '/api/student/';
    var classRoomsUri = '/api/classroom/';

    function ajaxHelper(uri, method, data) {
        self.error('');
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

    function getAllStudents() {
        ajaxHelper(studentsUri, 'GET').done(function (data) {
            self.students(data);
        });
    }

    self.getStudentDetail = function (item) {
        ajaxHelper(studentsUri + item.StudentID, 'GET').done(function (data) {
            self.detail(data);
        });
    }

    function getClassRooms() {
        ajaxHelper(classRoomsUri, 'GET').done(function (data) {
            self.classRooms(data);
        });
    }


    self.addStudent = function (formElement) {
        var student = {
            ClassRoomID: self.newStudent.ClassRoom().ClassRoomID,
            Name: self.newStudent.Name(),
            Surname: self.newStudent.Surname(),
            Adress: self.newStudent.Adress(),
            PhoneNumber: self.newStudent.PhoneNumber(),
            Age: self.newStudent.Age(),
            Gender: self.newStudent.Gender()
        };

        ajaxHelper(studentsUri, 'POST', student).done(function (item) {
            self.students.push(item);
        });
    }

    // Fetch the initial data.
    getAllStudents();
    getClassRooms();
};

ko.applyBindings(new ViewModel());