﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektASP.Validators
{
    public class AGEV : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if((int)value < 115 && (int)value > 10)
            {
                return ValidationResult.Success;
            }
            else return new ValidationResult("Wprowadź poprawny wiek");
        }
    }
}