﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektASP.Validators
{
    public class MARKV : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if ((int)value >= 0 && (int)value < 7)
            {
                return ValidationResult.Success;
            }
            else return new ValidationResult("Wprowadz poprawna ocene z zakresu 0 do 6, gdzie 0 oznacze ze student nie pisał");
        }
    }
}