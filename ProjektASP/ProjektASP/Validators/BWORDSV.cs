﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektASP.Validators
{
    public class BWORDSV : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!value.ToString().Contains("Kurwa"))
            {
                return ValidationResult.Success;
            }
            else return new ValidationResult("Wiadomosc nie moze zawierac wulgaryzmów");
        }
    }
}